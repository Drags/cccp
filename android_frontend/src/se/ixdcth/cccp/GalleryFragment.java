package se.ixdcth.cccp;

import java.util.ArrayList;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.actionbarsherlock.app.SherlockFragment;
 

public class GalleryFragment extends SherlockFragment implements OnItemClickListener {
	
	ImageAdapter list;
	
	@Override
	 public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		
		  	View myFragmentView = inflater.inflate(R.layout.fragment_gallery, container, false);
	 
	        super.onCreate(savedInstanceState);
	//		setContentView(R.layout.fragment_gallery);
	 
	        GridView gridView = (GridView) myFragmentView.findViewById(R.id.gridview);
	 
	        System.out.println("Gridview: " + gridView.toString());
	        // Instance of ImageAdapter Class
	        Context tempContext = inflater.getContext();
	        
	        list = new ImageAdapter(tempContext, MainActivity.getRunnerList());
	        System.out.println("Image adapter: " + list.toString());
	        
	        gridView.setAdapter(list);
	        gridView.setOnItemClickListener(this);
	        gridView.setPadding(0, 0, 0, 0);
	        gridView.setStretchMode(GridView.STRETCH_COLUMN_WIDTH);
	        
	        return myFragmentView;
		    
	 }

	void onFinishInflate(){
		
	}
	
//	@Override
//	public void onViewCreated(View view, Bundle savedInstanceState) {
//		super.onViewCreated(view, savedInstanceState);
//		getView().setOnItemClickListener(this);
//	}

	
	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int index, long id) {
		
		Runner runner = (Runner)list.getItem(index);
		
		RunnerActivity newFragment = new RunnerActivity();
		Bundle args = new Bundle();
		args.putInt("RUNNER_ID", runner.getRunnerID());
		newFragment.setArguments(args);

		FragmentTransaction transaction = getFragmentManager().beginTransaction();

		transaction.replace(R.id.fragment_container, newFragment);
		transaction.addToBackStack(null);

		// Commit the transaction
		transaction.commit();
		
	}

	private class ImageAdapter extends BaseAdapter {
	    private Context mContext;
	    private ArrayList<Runner> runnerList;
	 
	    // Keep all Images in array
//	    public Integer[] mThumbIds = {
//	    		R.drawable.sample_2, R.drawable.sample_3,
//	            R.drawable.sample_4, R.drawable.sample_5,
//	            R.drawable.sample_6, R.drawable.sample_7,
//	            R.drawable.sample_0, R.drawable.sample_1,
//	            R.drawable.sample_2, R.drawable.sample_3,
//	            R.drawable.sample_4, R.drawable.sample_5,
//	            R.drawable.sample_6, R.drawable.sample_7,
//	            R.drawable.sample_0, R.drawable.sample_1,
//	            R.drawable.sample_2, R.drawable.sample_3,
//	            R.drawable.sample_4, R.drawable.sample_5,
//	            R.drawable.sample_6, R.drawable.sample_7
//	    };
	 
	    // Constructor
	    public ImageAdapter(Context c, ArrayList<Runner> runners){
	        mContext = c;
	        this.runnerList = runners;
	    }
	 
	    @Override
	    public int getCount() {
	        return runnerList.size();
	    }
	 
	    @Override
	    public Object getItem(int position) {
	        return runnerList.get(position);
	    }
	 
	    @Override
	    public long getItemId(int position) {
	        return 0;
	    }
	 
	    @Override
	    public View getView(int position, View convertView, ViewGroup parent) {
	        ImageView imageView = new ImageView(mContext);
//	        imageView.setImageResource(mThumbIds[position]);
	        Runner runner = runnerList.get(position);
	        if(runner.getThumbnailImage().getDrawable() == null){
				MainActivity.getCommunicator().getRunnerThumbnail(runner, imageView);
			} else {
				imageView.setImageDrawable(runner.getThumbnailImage().getDrawable());
			}
	        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
	        
	        DisplayMetrics metrics = new DisplayMetrics();
	        getSherlockActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
	        imageView.setLayoutParams(new GridView.LayoutParams(metrics.widthPixels/4,metrics.widthPixels/4));
	        
	        imageView.setPadding(2, 2, 2, 2);
	        return imageView;
	    }
	 
	}
		    
		     
		    
		    
		
		

	
}
