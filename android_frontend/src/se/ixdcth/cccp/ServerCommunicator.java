package se.ixdcth.cccp;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.telephony.TelephonyManager;
import android.widget.ImageView;

/**
 * Makes requests and receives messages from the PHP server
 * 
 */
public class ServerCommunicator {

	private final static String API_HOST = ; //Insert your hostname here
	private final static int GET_RUNNER_LIST = 0;
	private final static int SPOT_RUNNER = 1;
	private final static int VOTE_RUNNER = 2;
	private final static int REGISTER_USER = 3;
	private MainActivity mainActivity;

	private String userID = "";

	/**
	 * Don't know what's needed here yet...
	 */
	public ServerCommunicator(MainActivity mainActivity) {
		this.mainActivity = mainActivity;
		String uid = null;
		if ((mainActivity.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE) {
			TelephonyManager tManager = (TelephonyManager) mainActivity.getSystemService(Context.TELEPHONY_SERVICE);
			uid = tManager.getDeviceId();
		}
		if (uid == null) {
			WifiManager wifiMan = (WifiManager) mainActivity.getSystemService(Context.WIFI_SERVICE);
			WifiInfo wifiInf = wifiMan.getConnectionInfo();
			uid = wifiInf.getMacAddress();
		}
		if (uid != null) {
			register(uid);
		} else {
			register("00000");
		}
	}

	/**
	 * Request basic list of runners from server Does not get images, just
	 * runner info (name, number avg. vote)
	 * 
	 * @return List of runners
	 */
	public boolean getRunnerList() {

		if (checkInternetConnection()) {
			new DownloadJsonString(GET_RUNNER_LIST).execute(API_HOST + "?rid=*");
		}

		return true;
	}

	/**
	 * Gets the thumbnail from the server and puts in the runner object
	 * 
	 * @param Runner
	 *            that needs a thumbnail
	 * @param viewToLoad
	 *            - extra view that can be notified when load is complete
	 * @return true when image is loaded or false at network error
	 */
	public boolean getRunnerThumbnail(Runner runner, ImageView viewToLoad) {
		if (checkInternetConnection()) {
			if (runner.getThumbnailImage().getDrawable() == null) {
				new DownloadImageTask(runner.getThumbnailImage(), viewToLoad).execute(API_HOST + "?rid=" + runner.getRunnerID() + "&img=2");
			}
			System.out.println("Image requested");
		}
		return true;
	}

	/**
	 * Gets the image from the server and puts in the runner object
	 * 
	 * @param Runner
	 *            that needs an image
	 * @param viewToLoad
	 *            - extra view that can be notified when load is complete
	 * @return true when image is loaded or false at network error
	 */
	public boolean getRunnerImage(Runner runner, ImageView viewToLoad) {
		if (checkInternetConnection()) {
			if (runner.getLargeImage().getDrawable() == null) {
				new DownloadImageTask(runner.getLargeImage(), viewToLoad).execute(API_HOST + "?rid=" + runner.getRunnerID() + "&img=1");
			}
		}
		return true;
	}

	/**
	 * Uploads a vote to the server
	 * 
	 * @param runner
	 *            containing scoring info
	 */
	public void uploadVote(Runner runner) {
		if (checkInternetConnection()) {
			new DownloadJsonString(VOTE_RUNNER, runner).execute(API_HOST + "?rid=" + runner.getRunnerID() + "&uid=" + userID + "&vote=" + runner.getUserVote());
		}
	}

	/**
	 * Uploads a spot to the server
	 * 
	 * @param bib
	 *            containing bib info
	 * @param runner
	 *            containing runner info
	 */
	public void uploadSpot(String bib, Runner runner) {
		if (checkInternetConnection()) {
			new DownloadJsonString(SPOT_RUNNER, runner).execute(API_HOST + "?uid=" + userID + "&spot=" + bib);
		}
	}

	/**
	 * parses a JSON string into a runner object and sends it to the
	 * MainActivity
	 * 
	 * @param jsonString
	 */
	private void parseRunnerList(String jsonString) {
		JSONObject jsonResult = null;
		if(jsonString == null){
			return;
		}
		try {
			jsonResult = new JSONObject(jsonString);
		} catch (JSONException e) {
			return;
		}

		if (jsonResult != null) {
			ArrayList<Runner> runnerList = MainActivity.getRunnerList();
			if (jsonResult.optBoolean("result")) {
				JSONArray innerField = jsonResult.optJSONArray("runners");

				for (int i = 0; i < innerField.length(); i++) {
					JSONObject tempObj = innerField.optJSONObject(i);
					Runner tempRunner = new Runner(tempObj.optInt("id"),
							tempObj.optString("name"),
							tempObj.optString("title"),
							tempObj.optString("bib"),
							tempObj.optDouble("score"),
							mainActivity);
					if (runnerList.contains(tempRunner)) {
						Runner listRunner = runnerList.get(runnerList.indexOf(tempRunner));
						listRunner.setAverageVote(tempRunner.getAverageVote());
					} else {
						runnerList.add(tempRunner);
					}
					// System.out.println(tempObj.optString("name") + "\n");
				}
				mainActivity.setRunnerList(runnerList);
				mainActivity.finishLoading();

			} else {
				System.out.println("Has JSONObject, but is no true");
			}
		} else {
			System.out.println("404: JSONObject not retreived.");
		}

	}

	/**
	 * Parses JSON return string from server after vote request
	 * 
	 * @param result
	 *            JSON result string
	 * @param runnerToUpdate
	 *            runner object to update
	 */
	private void setRunnerVoteStatus(String result, Runner runnerToUpdate) {
		JSONObject jsonResult = null;
		if(result == null){
			return;
		}
		try {
			jsonResult = new JSONObject(result);
		} catch (JSONException e) {
			return;
		}

		if (jsonResult != null) {
			if (jsonResult.optBoolean("result")) {
				runnerToUpdate.setVoteUploaded(true);
			} else {
				runnerToUpdate.setVoteUploaded(true);
				System.out.println("Vote failed to upload!!");
			}
		} else {
			System.out.println("404: JSONObject not retreived.");
		}

	}

	private void userRegistrationComplete(String result) {
		JSONObject jsonResult = null;
		if(result == null){
			return;
		}
		try {
			jsonResult = new JSONObject(result);
		} catch (JSONException e) {
			return;
		}

		if (jsonResult != null) {
			if (jsonResult.optBoolean("result")) {
				setUserID(jsonResult.optString("uid"));
			} else {
				System.out.println("Registration failed!");
			}
		} else {
			System.out.println("404: JSONObject not retreived.");
		}
	}

	/**
	 * Parses JSON return string from server after spot request
	 * 
	 * @param result
	 *            JSON result string
	 * @param runnerToUpdate
	 *            runner object to update
	 */
	private void setRunnerSpotStatus(String result, Runner runnerToUpdate) {
		JSONObject jsonResult = null;
		try {
			jsonResult = new JSONObject(result);
		} catch (JSONException e) {
			return;
		}

		if (jsonResult != null) {
			if (jsonResult.optBoolean("result")) {
				runnerToUpdate.setSpotUploaded(true);
			} else {
				// Should not happen unless hash has an epic fail.
				runnerToUpdate.setSpotUploaded(true);
				System.out.println("Spot not uploaded!!");
			}
		} else {
			System.out.println("404: JSONObject not retreived.");
		}

	}

	/**
	 * Gets an arbitrary InputStream from an URL
	 * 
	 * @param urlString
	 *            - URL to fetch
	 * @return InputStream from server
	 * @throws IOException
	 */
	private InputStream getInputStreamFromURL(String urlString) throws IOException {
		InputStream is = null;

		try {
			URL url = new URL(urlString);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setReadTimeout(10000 /* milliseconds */);
			conn.setConnectTimeout(15000 /* milliseconds */);
			conn.setRequestMethod("GET");
			conn.setDoInput(true);
			// Starts the query
			conn.connect();
			//int response = conn.getResponseCode();
			is = new BufferedInputStream(conn.getInputStream());

			// Convert the InputStream into a string
			// String contentAsString = readIt(is, len);
			return is;

			// Makes sure that the InputStream is closed after the app is
			// finished using it.
		} finally {
			if (is != null) {
				// is.close();
			}
		}

	}

	private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
		private ImageView picToLoad;
		private ImageView extraView;

		public DownloadImageTask(ImageView view, ImageView extraView) {
			picToLoad = view;
			this.extraView = extraView;
		}

		@Override
		protected Bitmap doInBackground(String... arg0) {
			try {
				return BitmapFactory.decodeStream(getInputStreamFromURL(arg0[0]));
			} catch (IOException e) {
				return null;
			}
		}

		@Override
		protected void onPostExecute(Bitmap result) {
			// Bitmap bitmap = BitmapFactory.decodeStream(result);
			picToLoad.setImageBitmap(result);
			picToLoad.setBackgroundResource(android.R.color.transparent);
			extraView.setImageBitmap(result);
		}

	}

	private class DownloadJsonString extends AsyncTask<String, Void, String> {
		private int downloadTask;
		private Runner runnerToUpdate;

		public DownloadJsonString(int downloadTask) {
			this.downloadTask = downloadTask;
		}

		public DownloadJsonString(int downloadTask, Runner runner) {
			this.downloadTask = downloadTask;
			this.runnerToUpdate = runner;
		}

		@Override
		protected String doInBackground(String... arg0) {
			try {
				return convertStreamToString(getInputStreamFromURL(arg0[0]));
			} catch (IOException e) {
				System.out.println("ow");
				return null;
			}
		}

		@Override
		protected void onPostExecute(String result) {
			// Controller thing for deciding what to do with received JSONString
			if (downloadTask == GET_RUNNER_LIST) {
				parseRunnerList(result);
			} else if (downloadTask == SPOT_RUNNER) {
				setRunnerSpotStatus(result, runnerToUpdate);
			} else if (downloadTask == VOTE_RUNNER) {
				setRunnerVoteStatus(result, runnerToUpdate);
			} else if (downloadTask == REGISTER_USER) {
				userRegistrationComplete(result);
			}
		}

	}

	// Reads an InputStream and converts it to a String. (Credit:
	// http://stackoverflow.com/questions/309424/read-convert-an-inputstream-to-a-string)
	static String convertStreamToString(java.io.InputStream is) {
		java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
		return s.hasNext() ? s.next() : "";
	}

	public void register(String uid) {
		new DownloadJsonString(REGISTER_USER).execute(API_HOST + "?register=" + uid);
	}

	/**
	 * @param i
	 *            the userID to set
	 */
	public void setUserID(String uid) {
		userID = uid;
	}

	public boolean checkInternetConnection() {
		ConnectivityManager connMgr = (ConnectivityManager) mainActivity.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		boolean isWifiConn = networkInfo.isConnected();
		if (isWifiConn) {
			return true;
		}
		networkInfo = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		boolean isMobileConn = networkInfo.isConnected();
		if (isMobileConn) {
			return true;
		}
		System.out.println("No internet connection!");
		return false;
	}

}
