package se.ixdcth.cccp;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;

public class RunnerActivity extends SherlockFragment {

	private Runner r;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View myFragmentView = inflater.inflate(R.layout.activity_runner, container, false);
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.activity_runner);
		Bundle extras = getArguments();
		
		int runnerID = extras.getInt("RUNNER_ID");
		
        r = MainActivity.getRunnerByID(runnerID);
        if(r != null){
        	//Set gui data
            TextView runnerTV = (TextView)myFragmentView.findViewById(R.id.runnerpage_name);
            runnerTV.setText(r.getRunnerName());
            
            ImageView runnerIV = (ImageView)myFragmentView.findViewById(R.id.runnerpage_image);
			if(r.getLargeImage().getDrawable() == null){
				MainActivity.getCommunicator().getRunnerImage(r, runnerIV);
			} else {
				runnerIV.setImageDrawable(r.getLargeImage().getDrawable());
			}
            
			RatingBar bar = (RatingBar)myFragmentView.findViewById(R.id.myvote);
			if(r.hasUserVote()){
				bar.setRating(r.getUserVote());
			}
			
			TextView globalRating = (TextView)myFragmentView.findViewById(R.id.runnerpage_vote);
			final double vote = r.getAverageVote();
			if (!Double.isNaN(vote)){
				globalRating.setText("Average rating: " + String.format("%1.1f", vote));
			} else {
				globalRating.setText("No votes yet. Be the first to vote!");
			}
	        
	        Button button = (Button) myFragmentView.findViewById(R.id.submitbutton);
			button.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					RatingBar bar = (RatingBar)getView().findViewById(R.id.myvote);
					r.setUserVote((int)bar.getRating());
					r.setVoteUploaded(false);
					if(MainActivity.getCommunicator().checkInternetConnection()){
						MainActivity.getCommunicator().uploadVote(r);
					}
					Toast toast = Toast.makeText(getActivity(),"Vote saved!", Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
					View view = toast.getView();
					view.setBackgroundColor(R.color.cccp_lightblue);
					TextView text = (TextView) view.findViewById(android.R.id.message);
					text.setBackgroundColor(R.color.cccp_lightblue);
					toast.show();
					
				}
			});
        }
		
        
        return myFragmentView;
	}
	
	@Override
	public void onPause() {
		super.onPause();
		//Remove fragment to make sure it's remade next time
		getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
	}


//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.runner, menu);
//		return true;
//	}

}
