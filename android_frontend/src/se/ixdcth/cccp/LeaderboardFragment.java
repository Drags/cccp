package se.ixdcth.cccp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockListFragment;

public class LeaderboardFragment extends SherlockListFragment implements OnItemClickListener {


	VoteList list;
	Context thisContext;
	View myFragmentView;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
	 	ArrayList<Runner> runners = new ArrayList<Runner>(MainActivity.getRunnerList());
	 	Collections.sort(runners, new RunnerLeadComparator());
	 	list = makeList(runners, inflater.getContext());
	 	thisContext = inflater.getContext();
	 	myFragmentView = inflater.inflate(R.layout.activity_leaderboard, container, false);
	 	setListAdapter(list);
	 	
	 	
	 	return myFragmentView;
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		getListView().setOnItemClickListener(this);
	}
	
	@Override
	public void onResume(){
		if(MainActivity.getCommunicator().checkInternetConnection()){
			MainActivity.getCommunicator().getRunnerList();
		}
	 	ArrayList<Runner> runners = new ArrayList<Runner>(MainActivity.getRunnerList());
	 	Collections.sort(runners, new RunnerLeadComparator());
	 	list = makeList(runners, thisContext);
	 	
	 	setListAdapter(list);
	 	super.onResume();
	}
	
	public void updateList(){
		if(myFragmentView != null){
		 	ArrayList<Runner> runners = new ArrayList<Runner>(MainActivity.getRunnerList());
		 	Collections.sort(runners, new RunnerLeadComparator());
		 	list = makeList(runners, thisContext);
		 	
		 	setListAdapter(list);
		}
	}
	
	private VoteList makeList(ArrayList<Runner> runnerList, Context context){
			
		VoteList returnList = new VoteList(context, runnerList);
		
		return returnList;
	}
	

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int index, long id){

//		//Send runner as a message
		Runner runner = (Runner)list.getItem(index);
		
		RunnerActivity newFragment = new RunnerActivity();
		Bundle args = new Bundle();
		args.putInt("RUNNER_ID", runner.getRunnerID());
		newFragment.setArguments(args);

		FragmentTransaction transaction = getFragmentManager().beginTransaction();

		transaction.replace(R.id.fragment_container, newFragment);
		transaction.addToBackStack(null);

		// Commit the transaction
		transaction.commit();
	}
		
	
		
	private class VoteList extends BaseAdapter{
		
		private ArrayList<Runner> votes;
		
		private LayoutInflater inflater;
		
		@SuppressWarnings("unused")
		private Context context;

		public VoteList(Context context, ArrayList<Runner> runners) {
		    this.context = context;
		    this.votes = runners;
		    inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		public int getCount() {
		    // TODO Auto-generated method stub
		    return votes.size();
		}

		public Object getItem(int position) {
		    // TODO Auto-generated method stub
		    return votes.get(position);
		}

		public long getItemId(int position) {
		    // TODO Auto-generated method stub
		    return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			
			View view;
			view = inflater.inflate(R.layout.leaderboard_cell, parent, false);
			
			//get the item that is supposed to be in the list cell
			Runner runner = votes.get(position);
			
			ImageView runnerImage = (ImageView) view.findViewById(R.id.runner_thumbnail);
			if(runner.getThumbnailImage().getDrawable() == null){
				MainActivity.getCommunicator().getRunnerThumbnail(runner, runnerImage);
			} else {
				runnerImage.setImageDrawable(runner.getThumbnailImage().getDrawable());
			}
			
			ImageView eye = (ImageView) view.findViewById(R.id.spot_eye_leaderboard);
			if(runner.isSpotted()){
				eye.setVisibility(View.VISIBLE);
			}
			//Get a specific layout element from the xml
			TextView name = (TextView) view.findViewById(R.id.leaderboard_runner_name);
			//Change values of stuff in the layout
			name.setText(runner.getRunnerName());
			
			TextView score = (TextView)view.findViewById(R.id.leaderboard_runner_score);
			final double vote = runner.getAverageVote();
			if (!Double.isNaN(vote)){
				score.setText("Average rating: " + String.format("%1.1f", vote));
			} else {
				score.setText("No votes yet. Be the first to vote!");
			}
	
			TextView standing = (TextView)view.findViewById(R.id.leaderboard_standing);
			standing.setText((position+1) + "");
			//First place
			if(position == 0){
				standing.setTextColor(getResources().getColor(R.color.cccp_salmon));
			}else if(position == 1){
				standing.setTextColor(getResources().getColor(R.color.cccp_darkblue));
			}else if(position == 2){
				standing.setTextColor(getResources().getColor(R.color.cccp_lightblue));
			}
			
			RatingBar userRating = (RatingBar)view.findViewById(R.id.leaderboard_userrating);
			int tempUserRating = runner.getUserVote();
			if(tempUserRating == -1){
//				userRating.setRating(0);
				userRating.setVisibility(RatingBar.INVISIBLE);
			}else{
				userRating.setRating(runner.getUserVote());
			}
					
					
			// Return the generated view
			return view;
		}

	}
	
	public class RunnerLeadComparator implements Comparator<Runner> {
	    @Override
	    public int compare(Runner o1, Runner o2) {
	    	double o1Avg = o1.getAverageVote();
	    	double o2Avg = o2.getAverageVote();

	    	//Handle NaN's (Not my Nan!)
	    	if(Double.isNaN(o1Avg) && Double.isNaN(o2Avg)){
	    		return 0;
	    	} else if(!Double.isNaN(o1Avg) && Double.isNaN(o2Avg)){
	    		return -1;
	    	} else if(Double.isNaN(o1Avg) && !Double.isNaN(o2Avg)){
	    		return 1;
	    	} else {
	    		if(o1Avg == o2Avg){
	    			return 0;
	    		} else if(o1Avg > o2Avg){
	    			return -1;
	    		} else {
	    			return 1;
	    		}
	    		
	    	}
	    	
	    }
	}


	
	 
}
