package se.ixdcth.cccp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;

public class SpotFragment extends SherlockFragment {


	protected static final int BIB_LENGTH_LIMIT = 6;

	private Runner lastRunnerFound;
	View thisObject;
	
	private TextView textView;
	
	private String enteredBib;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View myFragmentView = inflater.inflate(R.layout.activity_spot, container, false);
		textView = (TextView) myFragmentView.findViewById(R.id.bibInputField);
		enteredBib = "";
		textView.setText(padBibInput(enteredBib));
		setupDialPad(myFragmentView);
		setupRunnerPopup(myFragmentView);
		thisObject = myFragmentView;
		return myFragmentView;
	}
	
	protected void submitSpot(String bib) {		
		//Killswitch easteregg
		if(bib.equals("00000")){
			Activity mainActivity = getSherlockActivity();
			Intent intent = mainActivity.getIntent();
			mainActivity.finish();
			mainActivity.startActivity(intent);
		}

		if(bib.equals("11111")){
			MainActivity.getRunnerList().clear();
			Activity mainActivity = getSherlockActivity();
			Intent intent = mainActivity.getIntent();
			mainActivity.finish();
			mainActivity.startActivity(intent);
		}
		
		Runner runner = MainActivity.getRunnerFromBib(bib);
		if(runner == null){
			Toast toast = Toast.makeText(getSherlockActivity(), getSherlockActivity().getString(R.string.failed_spot) + " " + bib, Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
			View view = toast.getView();
			view.setBackgroundColor(R.color.cccp_lightblue);
			TextView text = (TextView) view.findViewById(android.R.id.message);
			text.setBackgroundColor(R.color.cccp_lightblue);
			toast.show();
			if(MainActivity.getCommunicator().checkInternetConnection()){
				((MainActivity)getSherlockActivity()).uploadAllRunnersToServer();
			}
		} else if(MainActivity.getCommunicator().checkInternetConnection()){
			if(getSherlockActivity().findViewById(R.id.spot_popup).isShown()){
				dismissRunnerPopup();
			}
			Toast toast = Toast.makeText(getSherlockActivity(), bib + " " + getSherlockActivity().getString(R.string.successful_spot), Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
			View view = toast.getView();
			view.setBackgroundColor(R.color.cccp_lightblue);
			TextView text = (TextView) view.findViewById(android.R.id.message);
			text.setBackgroundColor(R.color.cccp_lightblue);
			toast.show();
			runner.setSpotted(true);
			runner.setDecodedBib(bib);
			MainActivity.getCommunicator().uploadSpot(bib, runner);
			((MainActivity)getSherlockActivity()).uploadAllRunnersToServer();
			showRunnerPopup(runner);

		} else {
			if(getSherlockActivity().findViewById(R.id.spot_popup).isShown()){
				dismissRunnerPopup();
			}
			Toast toast = Toast.makeText(getSherlockActivity(), bib + " " + getSherlockActivity().getString(R.string.successful_spot), Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
			View view = toast.getView();
			view.setBackgroundColor(R.color.cccp_lightblue);
			TextView text = (TextView) view.findViewById(android.R.id.message);
			text.setBackgroundColor(R.color.cccp_lightblue);
			toast.show();
			runner.setDecodedBib(bib);
			runner.setSpotted(true);
			showRunnerPopup(runner);
		}
	}
	
	private void showRunnerPopup(Runner runner){
		lastRunnerFound = runner;
		SherlockFragmentActivity tempActivity = getSherlockActivity();
		ImageView runnerThumbnail = ((ImageView)tempActivity.findViewById(R.id.spot_found_thumbnail));
		runnerThumbnail.setImageResource(R.drawable.loadingthumbnail_rect);
		if(runner.getThumbnailImage().getDrawable() == null){
			MainActivity.getCommunicator().getRunnerThumbnail(runner, runnerThumbnail);
		} else {
			runnerThumbnail.setImageDrawable(runner.getThumbnailImage().getDrawable());
		}
		((RatingBar)tempActivity.findViewById(R.id.spot_found_ratingbar)).setRating(runner.getUserVote());
		((TextView)tempActivity.findViewById(R.id.spot_found_runner_name)).setText(runner.getRunnerName());


		Animation animation = new AlphaAnimation(0, 1);
		animation.setDuration(150);
		
		
		RelativeLayout popup = (RelativeLayout)tempActivity.findViewById(R.id.spot_popup);
		popup.setAnimation(animation);
				
		popup.setVisibility(View.VISIBLE);
	}
	
	private void dismissRunnerPopup(){
		final RelativeLayout popup = (RelativeLayout)getSherlockActivity().findViewById(R.id.spot_popup);

		Animation animation = new AlphaAnimation(1, 0);
		animation.setDuration(150);
		
		animation.setAnimationListener(new AnimationListener() {         
		    @Override
		    public void onAnimationEnd(Animation animation) {
		    	popup.setVisibility(View.GONE);
		    }

		    @Override
		    public void onAnimationRepeat(Animation animation) { }

		    @Override
		    public void onAnimationStart(Animation animation) { }
		});
		
		
		popup.setAnimation(animation);
		
		popup.setVisibility(View.INVISIBLE);
		
	}
	
	private void setupRunnerPopup(View view){
		Button button = (Button) view.findViewById(R.id.spot_submit_vote);
		button.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				dismissRunnerPopup();
				Toast toast = Toast.makeText(getSherlockActivity(),"Vote saved!", Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
				View view = toast.getView();
				view.setBackgroundColor(R.color.cccp_lightblue);
				TextView text = (TextView) view.findViewById(android.R.id.message);
				text.setBackgroundColor(R.color.cccp_lightblue);
				toast.show();
				lastRunnerFound.setUserVote( (int)((RatingBar)v.getRootView().findViewById(R.id.spot_found_ratingbar)).getRating());
				MainActivity.getCommunicator().uploadVote(lastRunnerFound);
			}
		});
	}

	private void setupDialPad(View view) {
		Button button = (Button) view.findViewById(R.id.dialButton0);
		button.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				inputBibDigit(0);
			}
		});

		button = (Button) view.findViewById(R.id.dialButton1);
		button.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				inputBibDigit(1);
			}
		});

		button = (Button) view.findViewById(R.id.dialButton2);
		button.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				inputBibDigit(2);
			}
		});

		button = (Button) view.findViewById(R.id.dialButton3);
		button.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				inputBibDigit(3);
			}
		});

		button = (Button) view.findViewById(R.id.dialButton4);
		button.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				inputBibDigit(4);
			}
		});

		button = (Button) view.findViewById(R.id.dialButton5);
		button.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				inputBibDigit(5);
			}
		});

		button = (Button) view.findViewById(R.id.dialButton6);
		button.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				inputBibDigit(6);
			}
		});

		button = (Button) view.findViewById(R.id.dialButton7);
		button.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				inputBibDigit(7);
			}
		});

		button = (Button) view.findViewById(R.id.dialButton8);
		button.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				inputBibDigit(8);
			}
		});

		button = (Button) view.findViewById(R.id.dialButton9);
		button.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				inputBibDigit(9);
			}
		});
		
		button = (Button) view.findViewById(R.id.dialButtonClear);
		button.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				clearBibDigit();
			}
		});

		button = (Button) view.findViewById(R.id.dialButtonOK);
		button.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				submitSpot(enteredBib);
				enteredBib = "";
				textView.setText(padBibInput(enteredBib));
			}
		});

		
	}

	protected void clearBibDigit() {
		if (enteredBib.length() == 0) {
			return;
		} else if (enteredBib.length() == 1) {
			enteredBib = "";
			textView.setText(padBibInput(""));
			return;
		}
		enteredBib = enteredBib.subSequence(0, enteredBib.length()-1).toString();
		textView.setText(padBibInput(enteredBib));
	}

	protected void inputBibDigit(int i) {
		if (enteredBib.length() < BIB_LENGTH_LIMIT) {
			// setting up the string for the textview
			enteredBib += "" + i;
			textView.setText(padBibInput(enteredBib));
		}
	}
	
	protected String padBibInput(String bibString) {
		if (bibString == null) return null;
		String result = "";
		for (int i = 0; i < bibString.length(); ++i) {
			result += " "+bibString.charAt(i) + " ";
		}
		for (int i = bibString.length(); i < BIB_LENGTH_LIMIT; ++i) {
			result += " _ ";
		}
		return result;
	}

	
}
