package se.ixdcth.cccp;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;

public class HelpActivity extends SherlockActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_help);
		ActionBar actionBar = getSupportActionBar();
		 
        // Show Actionbar Icon
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setIcon(R.drawable.abs__ic_ab_back_holo_light);
 
        // Hide Actionbar Title
        actionBar.setDisplayShowTitleEnabled(false);
        
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        Button button = (Button)findViewById(R.id.help_ok_button);
        button.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}
	
	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {

		finish();
		
		return super.onMenuItemSelected(featureId, item);
	}
}
