package se.ixdcth.cccp;


import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class MainActivity extends SherlockFragmentActivity{

	ActionBar.Tab spotTab,myVotesTab,leaderboardTab,galleryTab;
    SpotFragment spotFrag = new SpotFragment();
    MyVotesFragment myVotesFrag = new MyVotesFragment();
    LeaderboardFragment leaderboardFrag = new LeaderboardFragment();
    GalleryFragment galleryFrag = new GalleryFragment();

    
    private static ArrayList<Runner> runners = new ArrayList<Runner>();
    
    private static ServerCommunicator communicator; // initializing communicator

    
    ProgressDialog progress = null;
 
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	
        super.onCreate(savedInstanceState);
        
        communicator = new ServerCommunicator(this);
        
        setContentView(R.layout.activity_main);
 
        ActionBar actionBar = getSupportActionBar();
 
        // Hide Actionbar Icon
        actionBar.setDisplayShowHomeEnabled(false);
 
        // Hide Actionbar Title
        actionBar.setDisplayShowTitleEnabled(false);
 
        // Create Actionbar Tabs
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
 
        // Set Tab Icon and Titles
        spotTab = actionBar.newTab().setIcon(R.drawable.eye_icon);
        leaderboardTab = actionBar.newTab().setIcon(R.drawable.trophy_icon);
        galleryTab = actionBar.newTab().setIcon(R.drawable.galleryicon);
        myVotesTab = actionBar.newTab().setIcon(R.drawable.user_icon);
        //spotTab = actionBar.newTab().setText("Spot");
        //myVotesTab = actionBar.newTab().setText("My Votes");
        //leaderboardTab = actionBar.newTab().setText("Leaderboard");
        //galleryTab = actionBar.newTab().setText("Gallery");
 
        // Set Tab Listeners
        spotTab.setTabListener(new TabListener(spotFrag));
        myVotesTab.setTabListener(new TabListener(myVotesFrag));
        leaderboardTab.setTabListener(new TabListener(leaderboardFrag));
        galleryTab.setTabListener(new TabListener(galleryFrag));
 
        // Add tabs to actionbar
        actionBar.addTab(spotTab);
        actionBar.addTab(myVotesTab);
        actionBar.addTab(galleryTab);
        actionBar.addTab(leaderboardTab);
        
        
        
        
        progress = new ProgressDialog(this);
        progress.setTitle(getString(R.string.dialog_title_get_data_from_server));
        progress.setMessage(getString(R.string.dialog_body_get_data_from_server));
        progress.show();
        
        
        getRunnersFromServer();
        
    }
    
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	getSupportMenuInflater().inflate(R.menu.main, menu);
    	return true;
    }
    
    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
    	System.out.println("menu pressed: "+featureId + item.getTitle());
    	if (featureId == 0) {
			Intent intent = new Intent(this.getBaseContext(), HelpActivity.class);
			startActivity(intent);
    	}
    	return true;
    }
    
    /**
     * Sends fetch request on all runners from server
     */
    private void getRunnersFromServer(){
    	communicator.getRunnerList();	
    }
    
    /**
     * Get list of runners
     *
     */
    public static synchronized ArrayList<Runner> getRunnerList(){
    	return runners;
    }
    
    public static synchronized ServerCommunicator getCommunicator(){
    	return communicator;
    }
    
    /**
     * Set list of runners
     * @param runnerList
     */
    public void setRunnerList(ArrayList<Runner> runnerList){
    	runners = runnerList;
    	//TODO: Make threadsafe?
    }
    
    /**
     * Removes the loading screen at startup
     */
    public void finishLoading(){
    	if(progress.isShowing()){
	    	progress.dismiss();
	    	
	        //showing help as a first screen on first startup after loading database
	        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
	        if (preferences.getBoolean("firstRun", true)) {
				Intent intent = new Intent(this.getBaseContext(), HelpActivity.class);
				startActivity(intent);
				SharedPreferences.Editor editor = preferences.edit();
				editor.putBoolean("firstRun", false); // value to store
				editor.commit();
	        }
    	} else {
    		leaderboardFrag.updateList();
    	}
    }
    
    
    public static synchronized Runner getRunnerByID(int id){
    	for(Runner r : MainActivity.getRunnerList()){
    		if(r.getRunnerID() == id){
    			return r;
    		}
    	}
    	return null;
    }
    
    /**
     * Sets runner vote if bib number exists
     * @return True if bib exists and vote set, false if no such bib
     */
    public boolean setRunnerVoteFromBib(String number, int myVote){
    	//TODO: Test it
        String runnerHash = bibToHash(number);
        for(Runner tempRunner : runners){
        	if(runnerHash.equals(tempRunner.getBibNumber())){
        		tempRunner.setUserVote(myVote);
        		tempRunner.setSpotted (true);
        		System.out.println(tempRunner.getRunnerName());
        		return true;
        	}
        }
            
    	return false;
    }
    
    public static Runner getRunnerFromBib(String bibNumber){
    	String runnerHash = bibToHash(bibNumber);
        for(Runner tempRunner : runners){
        	if(runnerHash.equals(tempRunner.getBibNumber())){
        		return tempRunner;
        	}
        }
    	
    	return null;
    }
    
    public  static String bibToHash(String bibNumber){
        // Create MD5 Hash
        MessageDigest digest;
		try {
			digest = java.security.MessageDigest
				        .getInstance("MD5");
	
	        digest.update(bibNumber.getBytes());
	        byte messageDigest[] = digest.digest();
	
	        // Create Hex String
	        StringBuffer hexString = new StringBuffer();
	        for (int i = 0; i < messageDigest.length; i++) {
	            String h = Integer.toHexString(0xFF & messageDigest[i]);
	            while (h.length() < 2)
	                h = "0" + h;
	            hexString.append(h);
	        }
	        String userHash = hexString.toString();
	        return userHash;
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
    }
    
    /**
     * Uploads all votes to the server
     */
    public void uploadAllRunnersToServer(){
    	for(Runner tempRunner : runners){
    		if(tempRunner.hasUserVote() && !tempRunner.isVoteUploaded()){
    			communicator.uploadVote(tempRunner);
    		}
    		if(tempRunner.isSpotted() && !tempRunner.isSpotUploaded()){
    			communicator.uploadSpot(tempRunner.getDecodedBib(), tempRunner);
    		}
    	}
    }
    
   
    
}
