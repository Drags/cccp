package se.ixdcth.cccp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockListFragment;

public class MyVotesFragment extends SherlockListFragment implements OnItemClickListener {


	VoteList list;
	Context thisContext;
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
	 	ArrayList<Runner> runners = MainActivity.getRunnerList();
	 	Collections.sort(runners, new RunnerVoteComparator());
	 	thisContext = inflater.getContext();
	 	list = makeList(runners, inflater.getContext());
	 	
	 	View myFragmentView = inflater.inflate(R.layout.activity_my_votes, container, false);
	 	setListAdapter(list);
	 	
	 	
	 	return myFragmentView;
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		getListView().setOnItemClickListener(this);
	}
	
	@Override
	public void onResume(){
	 	ArrayList<Runner> runners = MainActivity.getRunnerList();

	 	Collections.sort(runners, new RunnerVoteComparator());
	 	list = makeList(runners, thisContext);
	 	
	 	setListAdapter(list);
	 	super.onResume();
	}
	
	private VoteList makeList(ArrayList<Runner> runnerList, Context context){
		
		ArrayList<Runner> myVotes = new ArrayList<Runner>();
		
		for(Runner r : runnerList){
			if(r.hasUserVote() || r.isSpotted()){
				myVotes.add(r);
			}
		}
		
		VoteList returnList = new VoteList(context, myVotes);
		
		
		
		return returnList;
	}
	

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int index, long id) {
		Runner runner = (Runner)list.getItem(index);
		
		RunnerActivity newFragment = new RunnerActivity();
		Bundle args = new Bundle();
		args.putInt("RUNNER_ID", runner.getRunnerID());
		newFragment.setArguments(args);

		FragmentTransaction transaction = getFragmentManager().beginTransaction();

		transaction.replace(R.id.fragment_container, newFragment);
		transaction.addToBackStack(null);

		// Commit the transaction
		transaction.commit();
		
	}
		
	
		
	private class VoteList extends BaseAdapter{
		
		private ArrayList<Runner> votes;
		
		private LayoutInflater inflater;
		
		@SuppressWarnings("unused")
		private Context context;

		public VoteList(Context context, ArrayList<Runner> runners) {
		    this.context = context;
		    this.votes = runners;
		    inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		public int getCount() {
		    // TODO Auto-generated method stub
		    return votes.size();
		}

		public Object getItem(int position) {
		    // TODO Auto-generated method stub
		    return votes.get(position);
		}

		public long getItemId(int position) {
		    // TODO Auto-generated method stub
		    return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			
			View view;
			view = inflater.inflate(R.layout.my_vote_cell, parent, false);
	
			//get the item that is supposed to be in the list cell
			Runner runner = votes.get(position);
			
			//Change values of stuff in the layout
			//Rating
			RatingBar myRating = (RatingBar) view.findViewById(R.id.myvotes_ratingbar);
			myRating.setRating(runner.getUserVote());
			if(!runner.hasUserVote() && runner.isSpotted()){
				AlphaAnimation alpha = new AlphaAnimation(0.15F, 0.15F);
				alpha.setDuration(0); // Make animation instant
				alpha.setFillAfter(true); // Tell it to persist after the animation ends
				myRating.startAnimation(alpha);
			}
			
			ImageView eye = (ImageView) view.findViewById(R.id.spot_eye);
			if(runner.isSpotted()){
				eye.setVisibility(View.VISIBLE);
			}
			
			//Name
			TextView name = (TextView) view.findViewById(R.id.myvotes_runner_name);
			name.setText(runner.getRunnerName());
			
			//Image
			ImageView image = (ImageView) view.findViewById(R.id.myvotes_thumbnail);
			if(runner.getThumbnailImage().getDrawable() == null){
				MainActivity.getCommunicator().getRunnerThumbnail(runner, image);
			} else {
				image.setImageDrawable(runner.getThumbnailImage().getDrawable());
			}
	
			
			//Overall:
			TextView overallRating = (TextView) view.findViewById(R.id.myvotes_ratinglabel);
			final double vote = runner.getAverageVote();
			if (!Double.isNaN(vote)){
				overallRating.setText("Average rating: " + String.format("%1.1f", vote));
			} else {
				overallRating.setText("No votes yet. Be the first to vote!");
			}
			
			// Return the generated view
			return view;
		}

	}
	
	public class RunnerVoteComparator implements Comparator<Runner> {
	    @Override
	    public int compare(Runner o1, Runner o2) {



	    	if((o1.isSpotted() && !o1.hasUserVote()) && (o2.isSpotted() && o2.hasUserVote())
	    			|| (o1.isSpotted() && !o1.hasUserVote()) && (!o2.isSpotted() && !o2.hasUserVote())){
	    		return -1;
	    	} else if((o1.isSpotted() && o1.hasUserVote()) && (o2.isSpotted() && !o2.hasUserVote())
	    			|| (!o1.isSpotted() && !o1.hasUserVote()) && (o2.isSpotted() && !o2.hasUserVote())){
	    		return 1;
	    	} else {
	    		return 0;
	    	}
	    	
	    }
	}


	
	 
}
