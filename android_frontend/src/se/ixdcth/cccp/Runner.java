package se.ixdcth.cccp;

import android.content.Context;
import android.widget.ImageView;


/**
 * Runner contains all the information relating to a
 * single runner, such as name, bib-number and pictures.
 * @author Nyx
 */
public class Runner {
	
	private int runnerID;
	private String runnerName;
	private String runnerTitle;
	private String bibNumber;
	private int userVote;
	private double averageVote;
	private boolean isSpotted;
	private ImageView thumbnailImage;
	private ImageView largeImage;
	
	private boolean spotUploaded;
	private boolean voteUploaded;
	private String decodedBib;
	
	
	/**
	 * Constructs a Runner object from basic data obtained by server
	 * @param runnerName
	 * @param bibNumber
	 * @param averageVote
	 */
	public Runner(int runnerID, String runnerName, String runnerTitle, String bibNumber, double averageVote, Context appContext){
		this.runnerID = runnerID;
		this.runnerName = runnerName;
		this.runnerTitle = runnerTitle;
		this.bibNumber = bibNumber;
		this.averageVote = averageVote;
		this.thumbnailImage = new ImageView(appContext);
		thumbnailImage.setBackgroundResource(R.drawable.loadingscreen);
		this.largeImage = new ImageView(appContext);
		largeImage.setBackgroundResource(R.drawable.loadingscreen);
		this.userVote = -1;
		isSpotted = false;
		spotUploaded = false;
		voteUploaded = false;
		decodedBib = "";
	}

	@Override
	public boolean equals(Object other){
		if(this.runnerID == ((Runner)other).runnerID){
			return true;
		}
		return false;
	}

	public boolean hasUserVote(){
		if(userVote == -1)
			return false;
		else
			return true;
	}
	
	public int getRunnerID() {
		return runnerID;
	}


	public void setRunnerID(int runnerID) {
		this.runnerID = runnerID;
	}


	public String getRunnerTitle() {
		return runnerTitle;
	}


	public void setRunnerTitle(String runnerTitle) {
		this.runnerTitle = runnerTitle;
	}


	public String getRunnerName() {
		return runnerName;
	}


	public void setRunnerName(String runnerName) {
		this.runnerName = runnerName;
	}


	public String getBibNumber() {
		return bibNumber;
	}


	public void setBibNumber(String bibNumber) {
		this.bibNumber = bibNumber;
	}


	public int getUserVote() {
		return userVote;
	}


	public void setUserVote(int userVote) {
		if(userVote == -1){
			this.userVote = 0; //"Hack" addressing if user presses vote before setting score
							   //The real solution would be to set if(-1) then set to 0 at all
							   //RatingBars...
		} else {
			this.userVote = userVote;
		}
	}


	public double getAverageVote() {
		return averageVote;
	}


	public void setAverageVote(double averageVote) {
		this.averageVote = averageVote;
	}


	public boolean isSpotted() {
		return isSpotted;
	}


	public void setSpotted(boolean isSpotted) {
		this.isSpotted = isSpotted;
	}


	public boolean isSpotUploaded() {
		return spotUploaded;
	}

	public void setSpotUploaded(boolean spotUploaded) {
		this.spotUploaded = spotUploaded;
	}

	public boolean isVoteUploaded() {
		return voteUploaded;
	}

	public void setVoteUploaded(boolean voteUploaded) {
		this.voteUploaded = voteUploaded;
	}

	public String getDecodedBib() {
		return decodedBib;
	}

	public void setDecodedBib(String decodedBib) {
		this.decodedBib = decodedBib;
	}

	public ImageView getThumbnailImage() {
		return thumbnailImage;
	}


	public void setThumbnailImage(ImageView tumbnailImage) {
		this.thumbnailImage = tumbnailImage;
	}


	public ImageView getLargeImage() {
		return largeImage;
	}


	public void setLargeImage(ImageView largeImage) {
		this.largeImage = largeImage;
	}
	

}
