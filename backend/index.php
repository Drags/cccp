<?php 

/*

CCCP
Voting application backend
Contributors:	Petras "Drags" Šukys
				
2013© Chalmers

API specification

API is accessed via crafted URL with GET parameters

http://<host_url>/?[param_name=value&]

List of possible parameters:

register [string]
	(Havent decided yet, with which data should identify voters (phone number, email account?)). Or if it is possible, device/app/user UUID from Android API. If so,
	this command will become obsolete, as uid will be enough. REF: http://android-developers.blogspot.se/2011/03/identifying-app-installations.html
	Register as a voter in the system. Returns your uid. If there was already a user with the same email/number, return the same uid.

uid [string]
	Your (as voter/spotter) user id, to login with the system.
		myvote [unsigned intr32, *]
			Returns your vote for specified runner. If parameter is equal to "*", returns list of all your votes.
			
		myspot [unsigned int32, *]
			Returns 'true' if you have spotted the specified runner. Returns 'false' otherwise. If parameter is equal to "*", returns all spots by you.
	
rid [unsigned int32, *]
	Returns runners name, title. If rid equals "*" returns list of all runners.
		img [unsigned int8]
			If specified, returns picture of the runner instead of name/title. Parameter set's the quality of the image: 1 = full resolution, 2 = thumbnail
		
	If "uid" is specified, these extra parameters are also valid:
		spot [unsigned int32]
			Attempt to register a spot of the runner. If it is correct, returns 'true'. Otherwise 'false'.
			
		vote [unsigned int8]
			Register vote for the runner.

*/

function errorJSON($message) {
	$arr = array('result' => 'false', 'error' => $message);
	header('Content-Type: application/json');
	echo json_encode($arr);
}

function successJSON($message) {
	// here goes the connection to DB info
	$arr = array('result' => 'true', 'message' => $message);
	header('Content-Type: application/json');
	echo json_encode($arr);
} 

$con=mysqli_connect("localhost","cccp","panther","cccp");
// Check connection
if (mysqli_connect_errno($con)) {
	// connection to MySQL failed
	jsonError("Failed to connect to the database. SQL Error:" . mysqli_error($con));
}

// Parsing primary GET parameters to PHP variables
$get_register = $_GET["register"];
$get_uid = $_GET['uid'];
$get_rid = $_GET['rid'];

// Parsing secondary GET parameters to PHP variables
$get_myvote = $_GET['myvote'];
$get_myspot = $_GET['myspot'];
$get_img = $_GET['img'];
$get_spot = $_GET['spot'];
$get_vote = $_GET['vote'];

// debugging comands
$get_debug = $_GET['debug'];

if (!empty($get_debug)) {
	if ($get_debug === "1d") {
		// debuging mode on, return whole database
		$users = mysqli_query($con, "SELECT * FROM users");
		$users_array = array();
		if (mysqli_num_rows($users) >= 1) {
			while($row = mysqli_fetch_array($users)) {
				$users_array[] = $row;
			}
		}
		$costumes = mysqli_query($con, "SELECT * FROM costumes");
		$costumes_array = array();
		if (mysqli_num_rows($costumes) >= 1) {
			while($row = mysqli_fetch_array($costumes)) {
				$costumes_array[] = $row;
			}
		}
		$votes = mysqli_query($con, "SELECT * FROM votes");
		$votes_array = array();
		if (mysqli_num_rows($votes) >= 1) {
			while($row = mysqli_fetch_array($votes)) {
				$votes_array[] = $row;
			}
		}
		
		$spots = mysqli_query($con, "SELECT * FROM spots");
		$spots_array = array();
		if (mysqli_num_rows($spots) >= 1) {
			while($row = mysqli_fetch_array($spots)) {
				$spots_array[] = $row;
			}
		}
		
		$debug_array = array(
			"users" => $users_array,
			"costumes" => $costumes_array,
			"votes" => $votes_array,
			"spots" => $spots_array
		);
		header('Content-Type: application/json');
		echo json_encode($debug_array);
		die("");
	}
}

if (!empty($get_register)) {
	// registering
	$query = "";
	if (filter_var($get_register, FILTER_VALIDATE_EMAIL)) {
		// registering with email
		$query = "SELECT id FROM users WHERE email='" . $get_register . "'";
		
	} else {
		// registering with phone
		$query = "SELECT id FROM users WHERE phone='" . $get_register . "'";
	}
	
	$result_users = mysqli_query($con, $query);
	if (mysqli_num_rows($result_users) == 1)  {
		$user_description = mysqli_fetch_array($result_users);
		$arr = array('result' => 'true', 'uid' => $user_description['id']);
		header('Content-Type: application/json');
		echo json_encode($arr);
	} else if (mysqli_num_rows($user) > 1) {
		errorJSON("CRITICAL: Found multiple users with the same identification data");
	} else {
		// user not found, registering
		if (mysqli_query($con, "INSERT INTO users (email, phone) VALUES ('" . $get_register . "', '" . $get_register . "')")) {
			// user registered, returning uid
			$arr = array('result' => 'true', 'uid' => mysqli_insert_id($con));
			header('Content-Type: application/json');
			echo json_encode($arr);
		} else {
			// failed to register user
			errorJSON("Failed to register the user");
		}
	}
	
} else if (!empty($get_uid) AND !empty($get_rid)) {
	if (empty($get_vote) AND empty($get_spot)) {
		// no vote or spot specified with both uid and rid.
		errorJSON("UID and RID have to go with either VOTE or SPOT");
	} else if (!empty($get_vote)) {
		// cast/update vote
		$my_cast = mysqli_query($con, "SELECT * FROM votes WHERE userID='" . $get_uid . "' AND runnerID='" . $get_rid . "'");
		if (mysqli_num_rows($my_cast) == 1) {
			if (mysqli_query($con, "UPDATE votes SET vote='" . $get_vote . "' WHERE userID='" . $get_uid . "' AND runnerID='" . $get_rid . "'")) {
				successJSON("Vote updated.");
			} else {
				errorJSON("Failed to update vote. SQL Error: " . mysqli_error($con));
			}
		} else if (mysqli_num_rows($my_cast) > 1){
			errorJSON("CRITICAL: More votes than allowed.");
		} else {
			// no vote for this runner so far, inserting
			if (mysqli_query($con, "INSERT INTO votes (userID, runnerID, vote) VALUES ('" . $get_uid . "', '" . $get_rid . "', '" . $get_vote . "')")) {
				successJSON("Vote casted.");
			} else {
				errorJSON("Failed to cast vote. SQL Error: " .mysqli_error($con));
			}
		}
	}
} else if (!empty($get_uid)) {

	if (!empty($get_spot)) {
		// try to spot runner
		$bib_check = mysqli_query($con, "SELECT * FROM costumes WHERE bib='" . $get_spot ."'");
		if ($bib_check) {
			if (mysqli_num_rows($bib_check) == 1) {
				$row = mysqli_fetch_array($bib_check);
				
				//check if already spotted
				$spot_check = mysqli_query($con, "SELECT * FROM spots WHERE userID='" . $get_uid . "' AND costumeID='" . $row['ID'] . "'");
				if (mysqli_num_rows($spot_check) >= 1) {
					successJSON("Runner already spotted!");
				} else if (mysqli_query($con, "INSERT INTO spots (userID, costumeID) VALUES ('" . $get_uid . "', '" . $row['ID'] . "')")) {
					successJSON("Runner " . $get_spot . " spotted succesfully!");
				} else {
					errorJSON(mysqli_error($con));
				}
			} else if (mysqli_num_rows($bib_check) > 1) {
				errorJSON("More runner's with this bib than one");
			} else {
				errorJSON("Incorrect runner's bib!");
			}
		} else {
			errorJSON(mysqli_error($con));
		}
	} else if (empty($get_myvote) AND empty($get_myspot)) {		
		// just uid specified doesn't give anything
		errorJSON("UID alone is not valid.");
	} else if (!empty($get_myvote)) {
		// requesting list of my votes
		$vote_query = "";
		if ($get_myvote === "*") {
			$vote_query = "SELECT runnerID, vote FROM votes WHERE userID='" . $get_uid . "'";
		} else {
			$vote_query = "SELECT runnerID, vote FROM votes WHERE userID='" . $get_uid . "' AND runnerID='" . $get_myvote . "'";
		}
		$myvote_result = mysqli_query($con, $vote_query);
		$myvotes_array = array();
		if (mysqli_num_rows($myvote_result) >= 1) {
			while($row = mysqli_fetch_array($myvote_result)) {
				$myvotes_array[] = array( "runner" => $row['runnerID'], "vote" => $row['vote']);
			}
			$votes_json = array(
				"result" => "true",
				"votes" => $myvotes_array
			);
			header('Content-Type: application/json');
			echo json_encode($votes_json);
		} else {
			errorJSON("No votes found.");
		}
	} else if (!empty($get_myspot)) {
		// requesting list of my votes
		$spot_query = "";
		if ($get_myspot === "*") {
			$spot_query = "SELECT runnerID FROM spots WHERE userID='" . $get_uid . "'";
		} else {
			$spot_query = "SELECT runnerID FROM spots WHERE userID='" . $get_uid . "' AND runnerID='" . $get_myspot . "'";
		}
		$myspot_result = mysqli_query($con, $spot_query);
		$myspots_array = array();
		if (mysqli_num_rows($myspot_result) >= 1) {
			while($row = mysqli_fetch_array($myspot_result)) {
				$myspots_array[] = array( "runner" => $row['runnerID']);		// TODO return more meaningful data not just runner id
			}
			$spots_json = array(
				"result" => "true",
				"spots" => $myspots_array
			);
			header('Content-Type: application/json');
			echo json_encode($spots_json);
		} else {
			errorJSON("No spoted runners found.");
		}
	}
} else if (!empty($get_rid)) {
	// returing general information about runner
	if (!empty($get_img)) {
		if ($get_rid === "*") {
			// IF requested images for ALL runners, will fail. Returns empty image.
			header("Content-type: image/jpg");
		} else {			
			// returning img with scale. NOTE: NOT JSON HERE. image/jpeg.
			// TODO remove image column from database
			$imageFile = "";
			if ($get_img === "1") {
				// full res image
				$remoteImage = "img/" . $get_rid . ".jpg";
			} else /*if ($get_img === "2")*/ {
				// thumb
				$remoteImage = "img/small/" . $get_rid . ".jpg";
			}
			
			if (!file_exists($remoteImage)) {
				if ($get_img === "1") {
					// full res image
					$remoteImage = "img/default.jpg";
				} else /*if ($get_img === "2")*/ {
					// thumb
					$remoteImage = "img/small/default.jpg";
				}
			}
			$imginfo = getimagesize($remoteImage);
			
			header("Content-type: " . $imginfo['mime']);
			readfile($remoteImage);
		}
	} else {
		// returning title/name
		if ($get_rid === "*") {
			// requested whole list of runners
			$runners_result = mysqli_query($con,"SELECT costumes.id, costumes.bib, costumes.title, costumes.name, AVG(votes.vote) AS score FROM costumes LEFT JOIN votes ON costumes.id=votes.runnerID GROUP BY costumes.id ORDER BY RAND(4)");
			$runners_array = array();
			while($row = mysqli_fetch_array($runners_result)) {
				$runners_array[] = array("id" => $row['id'], "title" => $row['title'], "name" => $row['name'], "bib" => md5($row['bib']) , "score" => $row['score']);
			}
			$runners_json = array(
				"result" => "true",
				"runners" => $runners_array
			);
			header('Content-Type: application/json');
			echo json_encode($runners_json);
		} else {
			// requested single runner
			$runners_result = mysqli_query($con,"SELECT costumes.title, costumes.name, costumes.bib, AVG(votes.vote) AS score FROM costumes INNER JOIN votes ON costumes.id=votes.runnerID AND costumes.id ='" . $get_rid . "'");
			if(!$runners_result) {
				errorJSON(mysqli_error($con));
			} else if (mysqli_num_rows($runners_result) == 1) {
				$row = mysqli_fetch_array($runners_result);
				$runners_array[] = array("id" => $get_rid, "title" => $row['title'], "name" => $row['name'], "bib" => md5($row['bib']) , "score" => $row['score']);
				$runners_json = array(
					"result" => "true",
					"runners" => $runners_array
				);
				header('Content-Type: application/json');
				echo json_encode($runners_json);
			} else if (mysqli_num_rows($runners_result) > 1) {
				header('Content-Type: application/json');
				echo json_encode($runners_json);
				//errorJSON("CRITICAL: More than one runner with specified RID.");
			} else {
				errorJSON("Runner with specified RID not found.");
			}
		}
		
	}
} else {
	errorJSON("Not a valid command.");
}

mysqli_close($con);
?>