-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.23 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             8.1.0.4545
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for cccp
CREATE DATABASE IF NOT EXISTS `cccp` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `cccp`;


-- Dumping structure for table cccp.costumes
CREATE TABLE IF NOT EXISTS `costumes` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `name` text,
  `bib` int(10) unsigned NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table cccp.spots
CREATE TABLE IF NOT EXISTS `spots` (
  `userID` int(10) unsigned DEFAULT NULL,
  `costumeID` int(10) unsigned DEFAULT NULL,
  KEY `FK__users` (`userID`),
  KEY `FK_spots_costumes` (`costumeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table cccp.users
CREATE TABLE IF NOT EXISTS `users` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phone` text,
  `email` text,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table cccp.votes
CREATE TABLE IF NOT EXISTS `votes` (
  `userID` int(10) unsigned NOT NULL DEFAULT '0',
  `runnerID` int(10) unsigned NOT NULL DEFAULT '0',
  `vote` int(10) unsigned NOT NULL DEFAULT '0',
  KEY `FK_votes_users` (`userID`),
  KEY `FK_votes_costumes` (`runnerID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
